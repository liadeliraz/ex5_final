<?php

	namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;
	use \yii\db\ActiveRecord;
	use yii\helpers\ArrayHelper;

	class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
	{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

	public function rules()
    {
        return [
            [['username', 'password'], 'string', 'max' => 255],
			[['username', 'password'], 'required'],
            [['username'], 'unique']			
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
        ];
    }

public static function getUsersWithAllUsers()
	{
		$users = self::getUsers();
		$users[-1] = 'All Users';
		$users = array_reverse ( $users, true );
		return $users;	
	}		
	
	public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }
	
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}

	/**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
		throw new NotSupportedException('You can only login
							by username/password pair for now.');
    }

	/**public function getFullName($username){
		
		return $this->getAttribute('username');
	}**/
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }
	
	
	public static function getUsers()
	{
		$users = ArrayHelper::
					map(self::find()->all(), 'id', 'username');
		return $users;						
	}
	
	
	
	
	

    public function validateAuthKey($authKey)
    {
         return $this->getAuthKey() === $authKey;
    }	

	public function validatePassword($password)
	{
		return $this->isCorrectHash($password, $this->password); 
	}

	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}

    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        if ($this->isNewRecord)
		    $this->auth_key = Yii::$app->security->generateRandomString(32);

        return $return;
    }
	
}