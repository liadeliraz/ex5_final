<?php

use yii\db\Migration;

class m160609_060137_alter_table_user extends Migration
{
   public function up()
    {
		$this->addColumn('users','firstname','string');
		$this->addColumn('users','lastname','string');
		$this->addColumn('users','email','string');
		$this->addColumn('users','phone','string');
		$this->addColumn('users','created_at','integer');
		$this->addColumn('users','updated_at','integer');
		$this->addColumn('users','created_by','integer');
		$this->addColumn('users','updated_by','integer');
    }

    public function down()
    {
        $this->dropColumn('users','firstname');
		$this->dropColumn('users','lastname');
		$this->dropColumn('users','email');
		$this->dropColumn('users','phone');
		$this->dropColumn('users','created_at');
		$this->dropColumn('users','updated_at');
		$this->dropColumn('users','created_by');
		$this->dropColumn('users','updated_by');

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}