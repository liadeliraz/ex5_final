<?php

use yii\db\Migration;

class m160515_115037_init_users_table extends Migration
{
    public function up()
    {
	 $this->createTable(
	   'users',
			[
			
			'id' => 'pk',
            'username' => 'string',
            'password' => 'string',
            'authKey' => 'string',
            ],
			 'ENGINE=InnoDB'
			);
	}
	   
	   
	   
    

    public function down()
    {
           $this->dropTable('users');

      
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
