<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Show One user';

?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        The name of the user is <?= $name ?>
    </p>
</div>
