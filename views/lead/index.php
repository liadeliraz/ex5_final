<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LeadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Leads';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Lead', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'status',
            'name',
            'email:email',
            'phone',
            'notes:ntext',
            // 'status',
            
             'created_at',
             'updated_at',
             'created_by',
             'updated_by',
				[ // the owner name of the lead
				'attribute' => 'owner',
				'label' => 'Owner',
				'format' => 'raw',
				'value' => function ($model){
					return Html::a($model->userOwner->fullname, 
					['user/view', 'id' => $model->userOwner->id]);
					},
					'filter'=>html::dropDownList('LeadSearch[owner]', $owner,
					$owners, ['class'=>'_form-control']),
						
			], 
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
